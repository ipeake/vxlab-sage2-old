// SAGE2 is available for use under the SAGE2 Software License
//
// University of Illinois at Chicago's Electronic Visualization Laboratory (EVL)
// and University of Hawai'i at Manoa's Laboratory for Advanced Visualization and
// Applications (LAVA)
//
// See full text, terms and conditions in the LICENSE.txt included file
//
// Copyright (c) 2014

"use strict";

/* global THREE */

function addScriptForThreejs(url, callback) {
	var script = document.createElement('script');
	if (callback) {
		script.onload = callback;
	}
	script.type = 'text/javascript';
	script.src  = url;
	document.body.appendChild(script);
}

var car_threejs_dynamic = SAGE2_App.extend({

	init: function(data) {
		this.SAGE2Init("div", data);

		this.resizeEvents = "onfinish";
		this.moveEvents   = "onfinish";

		this.renderer = null;
		this.camera   = null;
		this.scene    = null;
		this.ready    = null;

		this.cameraCube = null;
		this.sceneCube  = null;
		this.dragging   = null;
		this.rotating   = null;


		this.element.id = "div" + data.id;
		this.frame  = 0;
		this.width  = this.element.clientWidth;
		this.height = this.element.clientHeight;
		this.dragging = false;
		this.ready    = false;
		this.rotating = false;

		var _this = this;
		addScriptForThreejs(_this.resrcPath + "scripts/OrbitControls.js", function() {
			addScriptForThreejs(_this.resrcPath + "scripts/ctm/lzma.js", function() {
				addScriptForThreejs(_this.resrcPath + "scripts/ctm/ctm.js", function() {
					addScriptForThreejs(_this.resrcPath + "scripts/ctm/CTMLoader.js", function() {
						_this.initialize(data.date);
					});
				});
			});
		});

		this.controls.addButton({type: "prev", position: 1, identifier: "Left"});
		this.controls.addButton({type: "next", position: 7, identifier: "Right"});
		this.controls.addButton({type: "up-arrow", position: 4, identifier: "Up"});
		this.controls.addButton({type: "down-arrow", position: 10, identifier: "Down"});
		this.controls.addButton({type: "zoom-in", position: 12, identifier: "ZoomIn"});
		this.controls.addButton({type: "zoom-out", position: 11, identifier: "ZoomOut"});
		this.controls.addButton({type: "loop", position: 2, identifier: "Loop"});
		this.controls.finishedAddingControls();
	},

	initialize: function(date) {
		console.log("initialize ctm");

		// Allow cross-origin access, needed for wall to wall sharing
		THREE.ImageUtils.crossOrigin = 'anonymous';

		// CAMERA
		this.camera = new THREE.PerspectiveCamera(25, this.width / this.width, 1, 10000);
		this.camera.position.set(185, 40, 170);

		this.orbitControls = new THREE.OrbitControls(this.camera, this.element);
		this.orbitControls.maxPolarAngle = Math.PI / 2;
		this.orbitControls.minDistance = 200;
		this.orbitControls.maxDistance = 500;
		this.orbitControls.autoRotate  = false;
		this.orbitControls.zoomSpeed   = 0.1;
		this.orbitControls.autoRotateSpeed = 2.0; // 30 seconds per round when fps is 60

		// SCENE
		this.scene = new THREE.Scene();

		// SKYBOX
		this.sceneCube  = new THREE.Scene();
		this.cameraCube = new THREE.PerspectiveCamera(25, this.width / this.width, 1, 10000);
		this.sceneCube.add(this.cameraCube);

		var r    = this.resrcPath + "textures/";
		var urls = [ r + "px.jpg", r + "nx.jpg", r + "py.jpg", r + "ny.jpg", r + "pz.jpg", r + "nz.jpg" ];
		var textureCube = THREE.ImageUtils.loadTextureCube(urls);

		var shader = THREE.ShaderLib.cube;
		shader.uniforms.tCube.value = textureCube;

		var material = new THREE.ShaderMaterial({
			fragmentShader: shader.fragmentShader,
			vertexShader: shader.vertexShader,
			uniforms: shader.uniforms,
			depthWrite: false,
			side: THREE.BackSide
		});

		var mesh = new THREE.Mesh(new THREE.BoxGeometry(100, 100, 100), material);
		this.sceneCube.add(mesh);

		// LIGHTS

		var light = new THREE.PointLight(0xffffff, 1);
		light.position.set(2, 5, 1);
		light.position.multiplyScalar(30);
		this.scene.add(light);

		var light2 = new THREE.PointLight(0xffffff, 0.75);
		light2.position.set(-12, 4.6, 2.4);
		light2.position.multiplyScalar(30);
		this.scene.add(light2);

		this.scene.add(new THREE.AmbientLight(0x050505));

		// RENDERER
		this.renderer = new THREE.WebGLRenderer({ antialias: true });
		this.renderer.setSize(this.width, this.height);
		this.renderer.autoClear = false;

		this.element.appendChild(this.renderer.domElement);

		this.renderer.gammaInput  = true;
		this.renderer.gammaOutput = true;

		// Loader
		var start = Date.now();
		var loaderCTM = new THREE.CTMLoader(false);

		// Allow cross origin loading
		loaderCTM.crossOrigin = "anonymous";

		var position = new THREE.Vector3(-105, -78, -40);
		var scale    = new THREE.Vector3(30, 30, 30);

		var _this = this;
		var m, mm, i;
		loaderCTM.loadParts(_this.resrcPath + "camaro/camaro.js", function(geometries, materials) {
			// hackMaterials
			for (i = 0; i < materials.length; i++) {
				m = materials[i];
				if (m.name.indexOf("Body") !== -1) {
					mm = new THREE.MeshPhongMaterial({ map: m.map });
					mm.envMap  = textureCube;
					mm.combine = THREE.MixOperation;
					mm.reflectivity = 0.75;
					materials[i] = mm;
				} else if (m.name.indexOf("mirror") !== -1) {
					mm = new THREE.MeshPhongMaterial({ map: m.map });
					mm.envMap  = textureCube;
					mm.combine = THREE.MultiplyOperation;
					materials[i] = mm;
				} else if (m.name.indexOf("glass") !== -1) {
					mm = new THREE.MeshPhongMaterial({ map: m.map });
					mm.envMap = textureCube;
					mm.color.copy(m.color);
					mm.combine = THREE.MixOperation;
					mm.reflectivity = 0.25;
					mm.opacity = m.opacity;
					mm.transparent = true;
					materials[i] = mm;
				} else if (m.name.indexOf("Material.001") !== -1) {
					mm = new THREE.MeshPhongMaterial({ map: m.map });
					mm.shininess = 30;
					mm.color.setHex(0x404040);
					mm.metal = true;
					materials[i] = mm;
				}
				materials[i].side = THREE.DoubleSide;
			}

			for (i = 0; i < geometries.length; i++) {
				var amesh = new THREE.Mesh(geometries[i], materials[i]);
				amesh.position.copy(position);
				amesh.scale.copy(scale);
				_this.scene.add(amesh);
			}

			var end = Date.now();
			console.log("load time:", end - start, "ms");

		}, { useWorker: true });

		this.ready = true;

                // hack to change POV for multiple instances of same display in RMIT VxPortal gallery
                console.log("car3js displayUID ", displayUID);
		// five-portal arrangement
                if (displayUID.indexOf("10.234.2.66") === 0) {
                       console.log("car3js portal 6 ");
                       this.orbitControls.rotateLeft(0);
                }
                if (displayUID.indexOf("10.234.2.65") === 0) {
                       console.log("car3js portal 5 ");
                       this.orbitControls.rotateLeft(1.256);
                }
                if (displayUID.indexOf("10.234.2.64") === 0) {
                       console.log("car3js portal 4 ");
                       this.orbitControls.rotateLeft(2*1.256);
                }
                // draw!
                if (displayUID.indexOf("10.234.2.63") === 0) {
                       console.log("car3js portal 3 ");
                       this.orbitControls.rotateLeft(3*1.256);
                }
                //if (displayUID.indexOf("10.234.2.62") === 0) {
                //     console.log("car3js portal 2 ");
                       //this.orbitControls.rotateLeft(4*1.256);
                //}
                if (displayUID.indexOf("10.234.2.61") === 0) {
                       console.log("car3js portal 1 ");
                       this.orbitControls.rotateLeft(4*1.256);
                }

		// draw!
		this.resize(date);
	},

	load: function(date) {
		// nothing
	},

	draw: function(date) {
		if (this.ready) {
			this.orbitControls.update();
			this.cameraCube.rotation.copy(this.camera.rotation);

			this.renderer.clear();
			this.renderer.render(this.sceneCube, this.cameraCube);
			this.renderer.render(this.scene, this.camera);
		}
	},

	move: function(date) {
		//We need to update on move as per resize now?
		//this.log("app.move works!");
		
		//enable/disable while testing
		//this.resize(date);
	},
			
	resize: function(date) {
		//There appears to be a difference between resize with a middle mouse scroll and grabbing the drag handle here. 
		//With an update on move enabled, only the drag handle resize appears to exhibit the expected behaviour. 
		//With the update on move disabled, both the middle mouse resize and drag handle resize exhibit the expected behaviour.
		//
		//SAGE2 seems to update this.element.style.width/height internally on move/resize. The work below is a hack to articulate the gpu memory overload (lack of scaling) with the basic car_three.js app.
		
		this.width  = this.element.clientWidth;
		this.height = this.element.clientHeight;
		
		//Extra variables to compute local client actual viewport/view
		var local_x 				= 0;
		var local_y 				= 0;
		var local_width 			= 0;
		var local_height 			= 0;
		var local_viewport_offset_x = 0;
		var local_viewport_offset_y = 0;

		var logoutput = true;
		
		if (this.isVisible()) { //Don't compute if we don't need to
			
			//Work out the local top-left window coordinate
			if (this.sage2_x < ui.offsetX){
				if(logoutput) this.log("Tile boundary crossed, local_x = 0");
				
				local_x = 0;
			} else {
				local_x = this.sage2_x - ui.offsetX;
			}
			
			if (this.sage2_y < ui.offsetY){
				if(logoutput) this.log("Tile boundary crossed, local_y = 0");
				
				local_y = 0;
			} else {
				local_y = this.sage2_y - ui.offsetY;
			}			
			
			//Test for different states of canvas coverage in the client and compute the local width/height
			//completely within tile in x				
			if (this.sage2_x > ui.offsetX && (this.sage2_x + this.width) < (ui.offsetX + this.config.resolution.width)){
				if(logoutput) this.log("completely within tile in x");
				
				local_width 			= this.width;
				local_viewport_offset_x = 0;
			}
			//completely within tile in y				
			if (this.sage2_y > ui.offsetY && (this.sage2_y + this.height) < (ui.offsetY + this.config.resolution.height)){
				if(logoutput) this.log("completely within tile in y");
				
				local_height 			= this.height;
				local_viewport_offset_y = 0;
			}			
			
			//first tile in x
			if (this.sage2_x > ui.offsetX && (this.sage2_x + this.width) > (ui.offsetX + this.config.resolution.width)) {
				if(logoutput) this.log("first tile in x");
			
				local_width 			= this.config.resolution.width - local_x;
				local_viewport_offset_x = 0;
			} 
			//first tile in y
			if (this.sage2_y > ui.offsetY && (this.sage2_y + this.height) > (ui.offsetY + this.config.resolution.height)){
				if(logoutput) this.log("first tile in y");
			
				local_height 			= this.config.resolution.height - local_y;
				local_viewport_offset_y = 0;
			} 			
				
			//complete coverage in x				
			if (this.sage2_x < ui.offsetX && (this.sage2_x + this.width) > (ui.offsetX + this.config.resolution.width)){
				if(logoutput) this.log("complete coverage in x");
				
				local_width 			= this.config.resolution.width;
				local_viewport_offset_x = ui.offsetX - this.sage2_x;
			}
			//complete coverage in y				
			if (this.sage2_y < ui.offsetY && (this.sage2_y + this.height) > (ui.offsetY + this.config.resolution.height)){
				if(logoutput) this.log("complete coverage in y");
				
				local_height 			= this.config.resolution.height;
				local_viewport_offset_y = ui.offsetY - this.sage2_y;
			}			

			//last tile in x
			if (this.sage2_x < ui.offsetX && (this.sage2_x + this.width) < (ui.offsetX + this.config.resolution.width)) {
				if(logoutput) this.log("last tile in x");
				
				local_width 			= (this.sage2_x + this.width) - ui.offsetX;
				local_viewport_offset_x = this.width - local_width;
			}
			//last tile in y
			if (this.sage2_y < ui.offsetY && (this.sage2_y + this.height) < (ui.offsetY + this.config.resolution.height)) {
				if(logoutput) this.log("last tile in y");
				
				local_height 			= (this.sage2_y + this.height) - ui.offsetY;
				local_viewport_offset_y = this.height - local_height;
			}			
			
			if(logoutput) this.log("ui.offsetX:" + ui.offsetX + " sage2_x:" + Math.ceil(this.sage2_x) + " sage2_width:" + Math.ceil(this.sage2_width) + " clientWidth:" +  Math.ceil(this.element.clientWidth) + " elementWidth:" + this.element.style.width + " local_x:" + Math.ceil(local_x) + " local_width:" + Math.ceil(local_width));
			
			if(logoutput) this.log("ui.offsetY:" + ui.offsetY + " sage2_y:" + Math.ceil(this.sage2_y) + " sage2_height:" + Math.ceil(this.sage2_height) + " clientHeight:" + Math.ceil(this.height) + " local_y:" + Math.ceil(local_y) + " local_height:" + Math.ceil(local_height));
		}
		
		//original constant buffer size for all contexts
		//this.renderer.setSize(this.width, this.height);
		
		//variable buffer size (actual size) for each context
		this.renderer.setSize(local_width, local_height);
		
		//local div offset on parent canvas

		this.element.style.width 		= local_width + "px";
		this.element.style.height 		= local_height + "px";
		this.element.style.marginLeft 	= local_viewport_offset_x + "px";
		this.element.style.marginTop 	= local_viewport_offset_y + "px";
		
		//this.renderer.setViewport(local_viewport_offset_x, local_viewport_offset_y, local_width, local_height);
		//this.renderer.setViewport(0, 0, local_width, local_height);

		//if(logoutput) this.log("ElementWidth now:" + this.element.style.width);
			
		//if(logoutput) this.log("Info: Prog:" + this.renderer.info.memory.programs + " Geom:" + this.renderer.info.memory.geometries  + " Tex:" + this.renderer.info.memory.textures + " Info: Verts:" + this.renderer.info.render.vertices + " Faces:" + this.renderer.info.render.faces + " points:" + this.renderer.info.render.points);

		if(logoutput) this.log("Context: drawingBufferWidth:" + this.renderer.context.drawingBufferWidth + " drawingBufferHeight:" + this.renderer.context.drawingBufferHeight);

		//Keep the aspect ratio relative to the overall parent canvas 
		//but offset the view for each client to match the offset viewport
		this.camera.aspect = this.width / this.height;
		this.camera.setViewOffset(this.width, this.height, local_viewport_offset_x, local_viewport_offset_y, local_width, local_height);
		this.camera.updateProjectionMatrix();

		this.cameraCube.aspect = this.width / this.height;
		this.cameraCube.setViewOffset(this.width, this.height, local_viewport_offset_x, local_viewport_offset_y, local_width, local_height);
		this.cameraCube.updateProjectionMatrix();
				
		this.refresh(date);
	},

	event: function(eventType, position, user_id, data, date) {
		if (this.ready) {
			if (eventType === "pointerPress" && (data.button === "left")) {
				this.dragging = true;
				this.orbitControls.mouseDown(position.x, position.y, 0);
			} else if (eventType === "pointerMove" && this.dragging) {
				this.orbitControls.mouseMove(position.x, position.y);
				this.refresh(date);
			} else if (eventType === "pointerRelease" && (data.button === "left")) {
				this.dragging = false;
			}

			if (eventType === "pointerScroll") {
				this.orbitControls.scale(data.wheelDelta);
				this.refresh(date);
			}

			if (eventType === "keyboard") {
				if (data.character === " ") {
					this.rotating = !this.rotating;
					this.orbitControls.autoRotate = this.rotating;
					this.refresh(date);
				}
			}

			if (eventType === "specialKey") {
				if (data.code === 37 && data.state === "down") { // left
					this.orbitControls.pan(this.orbitControls.keyPanSpeed, 0);
					this.orbitControls.update();
					this.refresh(date);
				} else if (data.code === 38 && data.state === "down") { // up
					this.orbitControls.pan(0, this.orbitControls.keyPanSpeed);
					this.orbitControls.update();
					this.refresh(date);
				} else if (data.code === 39 && data.state === "down") { // right
					this.orbitControls.pan(-this.orbitControls.keyPanSpeed, 0);
					this.orbitControls.update();
					this.refresh(date);
				} else if (data.code === 40 && data.state === "down") { // down
					this.orbitControls.pan(0, -this.orbitControls.keyPanSpeed);
					this.orbitControls.update();
					this.refresh(date);
				}
			} else if (eventType === "widgetEvent") {
				switch (data.identifier) {
					case "Up":
						// up
						this.orbitControls.pan(0, this.orbitControls.keyPanSpeed);
						this.orbitControls.update();
						break;
					case "Down":
						// down
						this.orbitControls.pan(0, -this.orbitControls.keyPanSpeed);
						this.orbitControls.update();
						break;
					case "Left":
						// left
						this.orbitControls.pan(this.orbitControls.keyPanSpeed, 0);
						this.orbitControls.update();
						break;
					case "Right":
						// right
						this.orbitControls.pan(-this.orbitControls.keyPanSpeed, 0);
						this.orbitControls.update();
						break;
					case "ZoomIn":
						this.orbitControls.scale(4);
						break;
					case "ZoomOut":
						this.orbitControls.scale(-4);
						break;
					case "Loop":
						this.rotating = !this.rotating;
						this.orbitControls.autoRotate = this.rotating;
						break;
					default:
						console.log("No handler for:", data.identifier);
						return;
				}
				this.refresh(date);
			}
		}
	}

});
